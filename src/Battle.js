var React = require('react');
var PropTypes = require('prop-types');
var Link = require('react-router-dom').Link;

// Player preview UI
var PlayerPreview = (props) => {
  return (
    <div>
      <img 
        src={props.avatar}
        alt={'Avatar for ' + props.username}
      />
      <h2>@{props.username}</h2>

      <button
        className='reset'
        onClick={props.onReset.bind(null, props.id)}
      > Reset
      </button>
    </div>
  )
}

// Prop types
PlayerPreview.propTypes = {
  avatar: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onReset: PropTypes.func.isRequired
}

class PlayerInput extends React.Component {
  // Props
  constructor(props) {
    super(props);

    // Set state
    this.state = {
      username: ''
    }

    // Bind functions
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // When typing input
  handleChange(event) {
    var value = event.target.value;

    this.setState(() => {
      return {
        username: value
      }
    });
  }

  // When submitting input
  handleSubmit(event) {
    event.preventDefault(); // prevent from submitting to server

    this.props.onSubmit(
      this.props.id,
      this.state.username
    );
  }

  // UI
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label htmlFor='username'>
          {this.props.label}
        </label>
        <input
          id='username'
          placeholder='github username'
          type='text'
          autoComplete='off'
          value={this.state.username}
          onChange={this.handleChange}
        />
        <button 
          className='button'
          type='submit'
          disabled={!this.state.username}
          submit=''
        > Go!
        </button>
      </form>
    )
  }
}

// Define types
PlayerInput.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
}

// PlayerInput
PlayerInput.default = {
  label: 'Username'
}

class Battle extends React.Component {
  // Props
  constructor(props) {
    super(props);

    // Set states
    this.state = {
      playerOneName: '',
      playerTwoName: '',
      playerOneImage: null,
      playerTwoImage: null,
    }

    // Bind functions
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  // Handle submit function
  handleSubmit(id, username) {
    this.setState(() => {
      var newState = {};
      newState[id + 'Name'] = username;
      newState[id + 'Image'] = 'https://github.com/' + username + '.png?size=200';
      return newState;
    });
  }

  // Handle reset
  handleReset(id) {
    this.setState(() => {
      var newState = {};
      newState[id + 'Name'] = '';
      newState[id + 'Image'] = null;
      return newState;
    });
  }

  // UI
  render() {
    var match = this.props.match;
    var playerOneName = this.state.playerOneName;
    var playerTwoName = this.state.playerTwoName;
    var playerOneImage = this.state.playerOneImage;
    var playerTwoImage = this.state.playerTwoImage;

    return (
      <div>
        {!playerOneName && 
          <PlayerInput 
            id='playerOne'
            label='Player One'
            onSubmit={this.handleSubmit}
          />
        }

        {playerOneImage !== null && 
          <PlayerPreview
            avatar={playerOneImage}
            username={playerOneName}
            onReset={this.handleReset}
            id='playerOne'
          />
        }

        {!playerTwoName && 
          <PlayerInput
            id='playerTwo'
            label='Player Two'
            onSubmit={this.handleSubmit}
          />
        }

        {playerTwoImage !== null && 
          <PlayerPreview
            avatar={playerTwoImage}
            username={playerTwoName}
            onReset={this.handleReset}
            id='playerTwo'
          />
        }

        {playerOneImage && playerTwoImage && 
          <Link
            to={{
              pathname: match.url + '/results',
              search: '?playerOneName=' + playerOneName + '&playerTwoName=' + playerTwoName
            }}>
              Battle
          </Link>
        }
      </div>
    )
  }
}

module.exports = Battle;